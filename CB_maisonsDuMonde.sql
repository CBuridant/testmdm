# Test technique Maisons du Monde
# Caroline Buridant, octobre 2020


-- Première requête
select date, sum(prod_price*prod_qty) as ventes 
from transactions 
where date < '2020-01-01' and date > '2018-12-31'
group by date
order by date;

-- Deuxième requête
select t.client_id, 
sum(t.prod_price*t.prod_qty) filter (where p.product_type = 'MEUBLE') as "ventes_meuble",
sum(t.prod_price*t.prod_qty) filter (where p.product_type = 'DECO') as "ventes_deco"
from transactions as t join product_nomenclature as p on t.prod_id = p.product_id
where date < '2020-01-01' and date > '2018-12-31'
group by t.client_id;