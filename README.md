# Strings To Match app - Maisons du Monde technical test 
## Caroline Buridant, oct 2020

## What is this ?

Let's assume there is a collection of input strings and a collection of query strings.
This is a simple Python application intended to determine how many times each query string occurs in the list of input strings. 

## Files

The folder contains 2 .py files :
*  StringToMatch.py : this Python module contains the StringToMatch class. It allows to create a StringToMatch object to be used in the main file ;
* main.py : this file executes the code and uses the previous module. Please note that the input strings are passed in this file as an environment variable. If you want to adapt it, you should modify this variable. 

## How to use this

In the prompt, call the main.py file with the following command :
**python -m main ab, abc, ab**
where "ab, abc, ab" represents the collection of query strings you want to match.
The application returns a dictionary of the following type :
**{'ab': 2, 'abc': 0, 'ab': 2}**
with the respective occurencies for each query string. 