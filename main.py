# Strings to Match App - Maisons du Monde technical test
# Caroline Buridant, oct 2020

# Import packages & module
import sys
import argparse
from StringToMatch import StringToMatch

# Define the collection of strings
stringsInput = ['ab', 'ab', 'abc']


if 1 <= len(stringsInput) <= 1000 :

    if __name__ == '__main__':
        
        # Create class StringToMatch
        strings = StringToMatch(stringsInput)
        
        # Convert queries input into needed format
        parser = argparse.ArgumentParser()
        parser.add_argument("queries",
                    type=str)
        args = parser.parse_args()
        queries = args.queries.split(',')
        
        if 1 <= len(queries) <= 1000 :
            
            # Match strings and queries
            occurences = strings.matchingStrings(queries)
            
            # Format results to be displayed
            resPretty = dict(zip(queries, occurences))
            print(resPretty)
            
        else :
            sys.exit("Erreur : queries ne doit pas etre vide ou comporter plus de 1000 elements")
        
else:
    sys.exit("Erreur : strings ne doit pas etre vide ou comporter plus de 1000 elements")