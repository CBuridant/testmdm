# dockerfile

FROM python:3.7
MAINTAINER Caroline Buridant 

ADD main.py /
ADD StringToMatch.py /

# ENV var=['ab','ab','abc']

ENTRYPOINT ["python","./main.py"]