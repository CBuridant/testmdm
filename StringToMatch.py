# Strings to Match App - Maisons du Monde technical test
# Caroline Buridant, oct 2020

# Import packages
import re

# Defining class StringToMatch :
class StringToMatch:
    
    def __init__(self, strings):
        # Parameter "strings" : a list of separated strings
       self.strings = strings
       

    def matchingStrings(self, queries):
        # Parameter "queries" : a list of separated strings
        occurences = []                        # init result object
        stringsAll = ' '.join(self.strings)      # concatenate strings
        for elem in queries :
            elemOccur = re.findall(r"\b({0})\b".format(elem), stringsAll)   # find occurences for each query
            occurences.append(len(elemOccur))  # store results
        return occurences